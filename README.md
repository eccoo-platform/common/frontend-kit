---
== DEPRECATED ==
This repo is deprecated as of today (15/11/21).
== DEPRECATED ==
---

# Equal Care Co-op Frontend Toolkit

This project contains common frontend assets for use in constructing the Equal Care Co-op frontends.̦ It will, over time, include
assets of the following types:

* SCSS stylesheet partials
* Nunjucks templates
* Images

## Styles

The styles are provided as SCSS partials. There is entrypoint, which can be included in the project's scss with the following
statement.

```scss
@import '<path to node_modules>/eccoo-frontend-kit/style/all';
```

You will need to provide the correct path to the `node_modules` folder, or add `node_modules` to the search path (e.g. with the
`--load-path` CLI option), in which case the import can be reduced to `eccoo-frontend-kit/style/all`.

## Templates

These are defined as Nunjucks templates. To add these, you will need to customise your nunjucks environment:

```js
const { templatePath } = require('@eccoo-platform/frontend-kit');

nunjucks.configure([
  templatePath,
  join(__dirname, '..', 'views'),
], {});
```

The templates can now be referred to by path, as follows:

```
{% extends 'layout/_base.njk' %}
```

The package defines the following layouts:

* `layout/_base.njk` - root template defining the standard page layout
  Blocks:
    * body - inner content of the body
  Variables:
    * page_title - override of the page title
    * stylesheets - stylesheets to include
