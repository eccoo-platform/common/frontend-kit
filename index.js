const { join } = require('path');

const templatePath = join( __dirname, 'templates' );

module.exports = {
  templatePath,
};
