const sass = require('node-sass');

describe('stylesheet rendering', () => {
  let result = undefined;

  beforeEach(() => {
    result = sass.renderSync({
      file: 'test/test.scss',
      includePaths: [ 'node_modules', 'styles' ],
    });
  });

  it('should render a stylesheet that includes the entrypoint', () => {
    expect(result).toHaveProperty('css');
    expect(result).toHaveProperty('stats.includedFiles');
    expect(result.stats.includedFiles).toContain('styles/_all.scss');
  });
});
