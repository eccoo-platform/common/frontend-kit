const { templatePath } = require('..');
const nunjucks = require('nunjucks');
nunjucks.configure(templatePath, { autoescape: true });

describe('template extension', () => {
  describe('base template', () => {
    it('should extend the base template', () => {
      const result = nunjucks.renderString('{% extends "layout/_base.njk" %}');
      expect(result).toBeDefined();
    });
  });
});
